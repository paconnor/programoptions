#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>

#include "exceptions.h"
#include "Options.h"

#include <boost/exception/all.hpp>
#include <boost/property_tree/ptree.hpp>      

#include <TROOT.h>
#include <TFile.h>
#include <TChain.h>

[[ maybe_unused ]]
static const char * green = "\x1B[32m",
                  * red   = "\x1B[31m",
                  * bold  = "\e[1m",
                  * def   = "\e[0m";

using namespace std;

namespace po = boost::program_options;
namespace pt = boost::property_tree;
namespace fs = filesystem;

void example (const fs::path& input,
              const fs::path& output,
              const pt::ptree& config,
              const pair<int,int> slice)
{
    cout << __func__ << endl;
    cout << input << '\n' << output << endl;

    cout << config.get<float>("flags.R") << endl;
    cout << config.get<int  >("flags.year") << endl;
    cout << config.get<fs::path>("corrections.METfilters") << endl;
    cout << config.get<fs::path>("corrections.hotzones"  ) << endl;

    //return;

    auto fIn  = TFile::Open(input .c_str(), "READ");
    auto fOut = TFile::Open(output.c_str(), "RECREATE");

    TChain * oldchain = new TChain("events"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    //vector<RecJet> * recJets = nullptr;
    //oldchain->SetBranchAddress("recJets", &recJets);
    //vector<GenJet> * genJets = nullptr;
    //if (isMC)
    //    oldchain->SetBranchAddress("genJets", &genJets);
    //// TODO: load any branch you may need

    [[ maybe_unused ]]
    TTree * newtree = oldchain->CloneTree(0);

    //BOOST_THROW_EXCEPTION(Darwin::BadInput("Inconsistent meta-data and config", metainfo, config));

    //Looper looper(/* TODO */);
    //while (looper(next)) 
    for (long long i = 0; i < 10; ++i) {
        cout << i << endl;
        if (i == 5)
            BOOST_THROW_EXCEPTION(Darwin::AnomalousEvent("Crash while looping over the tree and due too missing btagging", nullptr, 0));
            // TODO: what if this happens in a functor that does not know anything of the tree?
    }
    fOut->Close();
    fIn->Close(); 
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        fs::path input, output;

        Options options("This is just a toy to play with the options and with the exceptions.",
                            Options::set_config | Options::set_splitting);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file")
               .arg<float>   ("R"         , "flags.R"               , "R parameter in jet clustering algorithm" )
               .arg<int  >   ("year"      , "flags.year"            , "year (20xx)"                             )
               .arg<fs::path>("hotzones"  , "corrections.hotzones"  , "location of hot zones in the calorimeter")
               .arg<fs::path>("METfilters", "corrections.METfilters", "location of list of MET filters to apply")
               // TODO: add pileup_latest.txt, etc.
               .arg<string>  ("label"     , "flags.label"           , "additional label (e.g. HIPM)"            );
        options(argc, argv);

        if (fs::is_directory(output)) output /= input.filename();

        const auto& config = options.config;
        const auto& slice = options.slice;

        example(input, output, config, slice);
    }
    catch (boost::exception& e) {
        cerr << red << boost::diagnostic_information(e);
        if (!Options::full_cmd.empty())
             cerr << "To reproduce error, you may run:\n" << Options::full_cmd << '\n';
        cerr << def;
        for (const auto&& obj: *(gROOT->GetListOfFiles()))
            dynamic_cast<TFile *>(obj)->Close();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
