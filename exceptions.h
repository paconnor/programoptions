#include <stdexcept>
#include <string>
#include <limits>

#include <TTree.h>

namespace Darwin {

////////////////////////////////////////////////////////////////////////////////
/// Generic exception to throw whenever an unexpected event configuration
/// occurs in an event loop. Generic information on the event is provided 
/// by printing the event from the tree with `TTree::Show()`.
struct AnomalousEvent : public std::runtime_error {

    TTree * tree;
    long long iEvent;

    AnomalousEvent (const char * error,
            TTree * t = nullptr,
            long long i = std::numeric_limits<long long>::max()) :
        runtime_error(error), tree(t), iEvent(i)
    { }

    const char * what () const noexcept
    {
        if (tree && iEvent < std::numeric_limits<long long>::max()) tree->Show(iEvent);
        return std::runtime_error::what();
    }

};

struct BadInput : public std::runtime_error {

    BadInput (const char * error /* TODO: add MetaInfo */) :
        runtime_error(error)
    { }

    const char * what () const noexcept
    {
        // TODO: dump MetaInfo, config file, etc.?
        return std::runtime_error::what();
    }

};

}
