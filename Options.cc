#include <cstdlib>

#include <iostream>
#include <iomanip>
#include <regex>
#include <system_error>
#include <filesystem>
#include <string>

#include <boost/version.hpp>
#include <boost/program_options.hpp>
#include <boost/exception/all.hpp>

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "Options.h"

using namespace std;
namespace fs = filesystem;
namespace po = boost::program_options; // used to read the options from command line
namespace pt = boost::property_tree;

[[ maybe_unused ]]
static const char * green = "\x1B[32m",
                  * red   = "\x1B[31m",
                  * bold  = "\e[1m",
                  * def   = "\e[0m";

string Options::full_cmd;

////////////////////////////////////////////////////////////////////////////////
/// Function used by Boost::PO to check if the file exists and is regular.
void Options::check_input (const fs::path& input)
{
    full_cmd += ' ' + fs::absolute(input).string();
    if (fs::exists(input)) return;
    BOOST_THROW_EXCEPTION(fs::filesystem_error("\e[1mBad input\e[0m", input, make_error_code(errc::invalid_argument)));
}

////////////////////////////////////////////////////////////////////////////////
/// Function used by Boost::PO to check if the file may already exist or is
/// a directory.
void Options::check_output (const fs::path& output)
{
    full_cmd += ' ' + output.string();
    if (!fs::exists(output)) return;

    if (fs::is_directory(output)) {
        if (fs::equivalent(output,"."))
            BOOST_THROW_EXCEPTION(fs::filesystem_error("\e[1mNot a valid output\e[0m", output, make_error_code(errc::invalid_argument)));
    }
    else
        cerr << red << "Warning: you are overwriting " << output << def << '\n';
}

////////////////////////////////////////////////////////////////////////////////
/// Parse environment variable in string.
///
/// Adapted from [stackexchange](https://codereview.stackexchange.com/questions/172644/c-environment-variable-expansion)
string Options::parse_env_var (string s)
{
    static const regex env_re{R"--(\$\{([^}]+)\})--"};
    smatch match;
    while (regex_search(s, match, env_re)) {
        auto const from = match[0];
        auto const name = match[1].str();
        auto const value = getenv(name.c_str());
        if (!value) {
            auto const what = "\e[1mEnvironment variable ${" + name + "} does not exist.\e[0m";
            BOOST_THROW_EXCEPTION(runtime_error(what));
        }
        s.replace(from.first, from.second, value);
    }
    return s;
} 

const char * Options::parse_env_var (const char * p)
{
    return parse_env_var(string(p)).c_str();
}

fs::path Options::parse_env_var (const fs::path& p)
{
    return fs::path(parse_env_var(p.string()));
}

////////////////////////////////////////////////////////////////////////////////
/// Function used by Boost::PO to enable standard output
void set_verbose (bool flag)
{
    if (!flag) cout.setstate(ios_base::failbit);
}

////////////////////////////////////////////////////////////////////////////////
/// Function used by Boost::PO to disable standard error
void set_silent (bool flag)
{
    if ( flag) cerr.setstate(ios_base::failbit); 
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor for PO:
/// - contains a parser for the help itself
/// - contains a parser for the options, like config file and verbosity
/// - and contains a parser for the input, output, and any other option
///   as positional arguments
Options::Options (const string& tuto, int params) :
    helper{"Helper"}, common{"Options"}, custom{"Positional options"},
    tutorial(tuto), slice{1,0}
{
    // first the helper
    helper.add_options()
        ("help,h", "Help screen (what you seeing right now)")
        ("example,e", "Print example of config")
        ("tutorial,t", "Explain how to use the command");

    // then the running options
    common.add_options()
        ("verbose,v", po::bool_switch()->default_value(false)->notifier(set_verbose), "Enable standard output stream (useful for debugging)")
        ("silent,s" , po::bool_switch()->default_value(false)->notifier(set_silent), "Disable standard error stream (not recommended)");
    if (params & set_config) common.add_options()
        ("config,c" , po::value<fs::path>(&config_file), "Configuration file in INFO or JSON format");
    if (params & set_splitting) common.add_options()
        ("nSplit,N" , po::value<int>(&slice.first )->default_value(slice.first ), "Number of slices")
        ("nNow,k"   , po::value<int>(&slice.second)->default_value(slice.second), "Index of present slice");

    // positional arguments should be defined by hand by the user with `Options::args()`
}

////////////////////////////////////////////////////////////////////////////////
/// Parser for help
void Options::parse_helper (int argc, const char * const argv[])
{
    po::options_description cmd_line;
    cmd_line.add(helper)
            .add(common); // only used to display the helper message, but not to parse

    po::command_line_parser parser{argc, argv};
    parser.options(helper) // parse only the helper, but no other option at this stage
          .allow_unregistered(); // ignore unregistered options

    // defines actions
    po::variables_map vm;
    po::store(parser.run(), vm);
    po::notify(vm); // necessary for config to be given the value from the cmd line

    if (vm.count("help") || argc == 1) {
        fs::path executable = argv[0];
        cout << bold << executable.filename().string() << theory_cmd << def << "\nwhere";
        for (const auto& option: custom.options())
            cout << '\t' << option->long_name() << " = " << option->description() << '\n';
        cout << cmd_line << endl;
             //<< "Boost " << BOOST_LIB_VERSION << endl; // TODO: rather show this in error code
        exit(EXIT_SUCCESS);
    }

    if (vm.count("tutorial")) {
        cout << tutorial << endl;
        exit(EXIT_SUCCESS);
    }

    if (vm.count("example")) { // TODO: print example (with arguments either from cmd line or from config)
        cout << "   ______________________\n"
             << "  < Oops! not ready yet! >\n"
             << "   ----------------------\n"
             <<"          \\   ^__^\n"
             <<"           \\  (oo)\\_______\n"
             << "              (__)\\       )\\/\\\n"
             << "                  ||----w |\n"
             << "                  ||     ||\n"
             << flush;
        exit(EXIT_SUCCESS);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Parser for generic options, such as the config, etc.
void Options::parse_common (int argc, const char * const argv[])
{
    po::command_line_parser parser{argc, argv};
    parser.options(common)
          .allow_unregistered();

    po::variables_map vm;
    po::store(parser.run(), vm);
    po::notify(vm);

    if (vm.count("config")) {
        if      (config_file.extension() == ".json") pt::read_json(config_file.c_str(), config);
        else if (config_file.extension() == ".xml" ) pt::read_xml (config_file.c_str(), config);
        else                                         pt::read_info(config_file.c_str(), config);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Parser for options provided with `args()`, defined differently in each
/// application.
void Options::parse_custom (int argc, const char * const argv[])
{
    po::options_description cmd_line;
    cmd_line.add(common) // only used to dis`allow_unregistered()`
            .add(custom);

    po::command_line_parser parser{argc, argv};
    parser.options(cmd_line)
          .positional(pos_hide);

    po::variables_map vm;
    po::store(parser.run(), vm);

    if (!fs::exists(config_file))
    for (auto& name: names) {
        cout << name << endl;
        if (vm.count(name)) continue;
        throw po::required_option(name);
    }

    fs::path executable = argv[0];
    full_cmd = absolute(executable).string();
    full_cmd += " -v";

    po::notify(vm); // necessary for config to be given the value from the cmd line
    for (auto& longname: longnames) {
        auto arg = config.get_optional<string>(longname);
        if (!arg) continue;
        full_cmd += ' ' + *arg;
    }
    full_cmd = parse_env_var(full_cmd);
}

void Options::parse_config (pt::ptree& tree, string key) const
{
    // loop over the config and parse environment variables
    for (auto& it: tree) {
        string subkey = it.first;
        pt::ptree& child = it.second;
        auto value = tree.get_optional<string>(subkey);
        string newkey = key + (key.empty() ? "" : ".") + subkey;
        if (value) {
            *value = parse_env_var(*value);
            tree.put<string>(subkey, *value);
        }
        parse_config(child, newkey);
    }
}

void Options::operator() (int argc, const char * const argv[])
{
    try {
        parse_helper(argc, argv);
        parse_common(argc, argv);
        parse_custom(argc, argv);
        parse_config(config);
    }
    catch (const po::error& e) {
        cerr << red << e.what() << def << '\n';
        BOOST_THROW_EXCEPTION(runtime_error("Parsing command line has failed"));
    }
}

Options& Options::set (const char * name, const po::value_semantic * s,
                                    const char * desc)
{
    theory_cmd += ' '; theory_cmd += name;
    pos_hide.add(name, 1);
    custom.add_options()(name, s, desc);
    return *this;
}

Options& Options::input (const char * name, fs::path * file, const char * desc)
{
    longnames.push_back(name);
    const po::value_semantic * s =
        po::value<fs::path>(file)->notifier(check_input)->required();
    return set(name, s, desc);
}

Options& Options::output (const char * name, fs::path * file, const char * desc)
{
    longnames.push_back(name);
    const po::value_semantic * s =
        po::value<fs::path>(file)->notifier(check_output)->required();
    return set(name, s, desc);
}
