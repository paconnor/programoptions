ROOTINC    = $(shell root-config --incdir)
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)

BOOSTINC ?= /home/connorpa/anaconda3/include
BOOSTLIBS ?= /home/connorpa/anaconda3/lib

CFLAGS=-g -Wall -O3 -std=c++1z -fPIC -DDEBUG

all: libOptions.so example test

example: example.cc Options.h
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -L$(BOOSTLIBS) -lboost_system -lboost_program_options $(ROOTLIBS) -o $@ -L. -Wl,-rpath,. -lOptions

test: test.cc Options.h
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -L$(BOOSTLIBS) -lboost_timer -lboost_system -lboost_program_options $(ROOTLIBS) -o $@ -L. -Wl,-rpath,. -lOptions -lrt
	DARWIN_LFS=$(PWD) ./test -l all
	echo Done

libOptions.so: Options.cc Options.h
	g++ ${CFLAGS} -I$(BOOSTINC) -shared $< -L$(BOOSTLIBS) -lboost_system -lboost_program_options -o $@

clean: 
	rm -f *.pdf *.so *.o example test

.PHONY: all clean
