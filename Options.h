#ifndef __H_OPTIONS__
#define __H_OPTIONS__

#include <functional>
#include <vector>
#include <filesystem>
#include <iostream>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/property_tree/ptree.hpp>      

class Options {

    // internal parsing
    boost::program_options::options_description helper, //!< to display the helper
                                                common, //!< generic + explicit options like `--verbose`
                                                custom; //!< for positional arguments, depending on the actual command
    boost::program_options::positional_options_description pos_hide; //!< parser for positional arguments
    void parse_helper (int, const char * const []);
    void parse_common (int, const char * const []);
    void parse_custom (int, const char * const []);
    void parse_config (boost::property_tree::ptree&, std::string = "") const;

    std::filesystem::path config_file; //!< path to INFO, JSON, or XML config file

    // for helper
    const std::string tutorial;
    std::string theory_cmd;
    std::vector<std::string> names, longnames;

    // some semantic checks on input and output files
    static void check_input  (const std::filesystem::path&);
    static void check_output (const std::filesystem::path&);

    // generic code to add options
    Options& set (const char *, const boost::program_options::value_semantic *, const char *);
                        //Options& (Options::*)(const fs::path&));
    template<typename T> std::function<void(T)> put (const char * longname)
    {
        return [longname,this](T value) { config.put<T>(longname, value); };
    }

public:

    // constructor
    enum { set_none, set_config, set_splitting };
    Options (const std::string&, int = set_none);

    // adding options
    Options& input  (const char *, std::filesystem::path *, const char *);
    Options& output (const char *, std::filesystem::path *, const char *);
    template<typename T> Options& arg (const char * name, const char * longname, const char * desc)
    {
        names.push_back(name);
        longnames.push_back(longname);
        const boost::program_options::value_semantic * s =
            boost::program_options::value<T>()->notifier(put<T>(longname));
        return set(name, s, desc);
    }

    // parsing
    void operator() (int, const char * const []);
    static std::string           parse_env_var (std::string);
    static const char *          parse_env_var (const char *);
    static std::filesystem::path parse_env_var (const std::filesystem::path&);

    // result of parsing
    static std::string full_cmd;
    boost::property_tree::ptree config;
    std::pair<int,int> slice; //!< [ #slices, index of present slice ]
};
#endif
