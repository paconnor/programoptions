#define BOOST_TEST_MODULE test_options
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include "Options.h"

#include <iostream>
#include <vector>
#include <filesystem>

#include <TFile.h>

using namespace std;
namespace fs = std::filesystem;

vector<const char *> tokenize (const char * str)
{
    cout << "\e[1m" << str << "\e[0m" << endl;
    char * cmd = new char[strlen(str)+1];
    strcpy(cmd, str);
    vector<const char *> args;
    for (char * arg = strtok(cmd, " ");
                arg != nullptr;
                arg = strtok(NULL, " "))
        args.push_back(arg);
    //free(cmd); // do NOT free the memory
    return args;
}

BOOST_AUTO_TEST_SUITE( test_options )

    BOOST_AUTO_TEST_CASE( test_default )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test") );

        Options options("Test"); // no config, no splitting by default
        BOOST_TEST( options.full_cmd.empty() );
        BOOST_TEST( options.config.empty() );

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        {
            // testing normal commands
            auto const args = tokenize("exec -v input.root output.root");
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
            BOOST_TEST( input == "input.root" );
            BOOST_TEST( output == "output.root" );
            cout << options.full_cmd << endl;
            BOOST_TEST( !options.full_cmd.empty() );
            BOOST_TEST( options.config.empty() );
        }

        fs::remove_all("output.root");

        {
            // testing bad input
            auto const args = tokenize("exec -v inpu.root output.root");
            BOOST_CHECK_THROW( options(args.size(),args.data()), boost::exception );
        }

        {
            // testing bad output
            auto const args = tokenize("exec -v input.root .");
            BOOST_CHECK_THROW( options(args.size(),args.data()), boost::exception );
        }

        fs::remove_all("output.root");
    }

    BOOST_AUTO_TEST_CASE( test_splitting )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test", Options::set_splitting) );
        Options options("Test", Options::set_splitting);

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        // testing normal commands
        auto const args = tokenize("exec -v input.root output.root -N 42 -k 24");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
        BOOST_TEST( options.slice.first  == 42 );
        BOOST_TEST( options.slice.second == 24 );
        BOOST_TEST( options.config.empty() );

        fs::remove_all("output.root");
    }

    BOOST_AUTO_TEST_CASE( test_parse_env_var )
    {
        BOOST_CHECK_NO_THROW( Options::parse_env_var("${HOME}") );
        BOOST_CHECK_THROW( Options::parse_env_var("${THISVARDOESNOTEXIST}"), boost::exception );
    }

    BOOST_AUTO_TEST_CASE( test_config )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test", Options::set_config) );
        Options options("Test", Options::set_config);

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        // testing normal commands
        auto const args = tokenize("exec -v input.root output.root -c config.info");
        BOOST_TEST_REQUIRE( Options::parse_env_var("${DARWIN_LFS}") ); // is used in `config.info`
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
        BOOST_TEST( !options.config.empty() );
        BOOST_TEST( options.config.count("flags") );
        BOOST_TEST( options.config.count("corrections") );

        auto const flags = options.config.get_child("flags");
        BOOST_TEST( flags.count("isMC") );
        BOOST_TEST( flags.count("year") );
        BOOST_TEST( flags.count("HIPM") );
        BOOST_TEST( flags.count("R") );

        auto const corrections = options.config.get_child("corrections");
        BOOST_TEST( corrections.count("METfilters") );
        BOOST_TEST( corrections.count("hotzones") );
        BOOST_TEST( corrections.count("lumi") );
        BOOST_TEST( corrections.count("xsecs") );
        BOOST_TEST( corrections.count("JES") );
        BOOST_TEST( corrections.count("triggers") );
        BOOST_TEST( corrections.count("PUstaub") );

        fs::remove_all("output.root");
    }

BOOST_AUTO_TEST_SUITE_END()

#define DOXYGEN_SHOULD_SKIP_THIS
#include "example.cc"

BOOST_AUTO_TEST_SUITE( test_example )

    BOOST_AUTO_TEST_CASE( event_loop_crash )
    {
        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;

        Options options("This is just a toy to play with the options and with the exceptions.",
                            Options::set_config | Options::set_splitting);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file")
               .arg<float>   ("R"         , "flags.R"               , "R parameter in jet clustering algorithm" )
               .arg<int  >   ("year"      , "flags.year"            , "year (20xx)"                             )
               .arg<fs::path>("hotzones"  , "corrections.hotzones"  , "location of hot zones in the calorimeter")
               .arg<fs::path>("METfilters", "corrections.METfilters", "location of list of MET filters to apply")
               .arg<string>  ("label"     , "flags.label"           , "additional label (e.g. HIPM)"            );

        auto const args = tokenize("example -v input.root output.root -c config.info -N 42 -k 24");
        options(args.size(), args.data());

        if (fs::is_directory(output)) output /= input.filename();

        const auto& config = options.config;
        const auto& slice = options.slice;

        BOOST_CHECK_THROW( example(input, output, config, slice), boost::exception );

        fs::remove_all("output.root");
    }

BOOST_AUTO_TEST_SUITE_END()

